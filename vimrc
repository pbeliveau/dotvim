" plugins
call pathogen#helptags()
execute pathogen#infect()
filetype plugin indent on
syntax enable

" leader
let mapleader=","
let maplocalleader = "\\"

" generic custom
colorscheme seoul256-light
set autoindent
set shiftwidth=4
set cursorline
set modifiable
set number
set relativenumber
set belloff=all
set noautoread
set noautowrite

" specific custom
set fileencoding=utf-8
set foldenable
set breakindent
set linebreak
set nowrap
set textwidth=80

" backup, undo and crypt
set backupdir=~/documents/backup/
set backup
set cm=blowfish2
set history=1000
set undofile undodir=~/.tmp/undo//
set undofile
set undolevels=1000
set undoreload=10000

" search
setglobal hlsearch ignorecase smartcase incsearch
set hlsearch 
set ignorecase 
set smartcase 
set incsearch

" Markdown
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

" ledger vimrc
let g:ledger_bin = 'ledger'
let g:ledger_fold_blanks = 0
let g:ledger_maxwidth = 80
let g:ledger_decimal_sep = '.'
let g:ledger_default_commodity = '$'
let g:ledger_align_at = 60
let g:ledger_fillstring = '    -'
let g:ledger_detailed_first = 1
let g:ledger_extra_options = '--pedantic --explicit --check-payees'
au FileType ledger inoremap <silent> <Tab> <C-r>=ledger#autocomplete_and_align()<CR>
au FileType ledger vnoremap <silent> <Tab> :LedgerAlign<CR>

" NERDtree
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let g:NERDTreeWinSize = 18

" spellchecking
nmap <silent> <leader>s :set spell!<CR>

" add support for French letters without changing keyword layout
map! ;a à
map! ;A À
map! ;z â
map! ;c ç
map! ;C Ç
map! ;d è
map! ;f ê
map! ;F Ê
map! ;e é
map! ;E É
map! ;i î
map! ;o ô
map! ;O Ô
imap ;q «  »<Esc>hi
map! ;u ù
map! ;j û

" vimwiki related
""" vimwiki to markdown
let g:vimwiki_list = [{'path': '~/notebook/',
		     \ 'syntax': 'markdown', 'ext': '.md'}]

""" calendar function to call upon calendar.vim
function! OpenCalendar()
    execute ":Calendar -view=year -split=vertical -witdh=27"
endfunction
nmap <silent> <leader>c :call OpenCalendar()

" remapping navigations split
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" let NERDTree show hidden files by default
let NERDTreeShowHidden=1

" lightline support and settings
set laststatus=2
set noshowmode
if !has('gui_running')
    set t_Co=256
endif
let g:lightline = {
	\ 'colorscheme': 'seoul256',
	\ }

" function to empty the registers
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor

" Save folds
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
