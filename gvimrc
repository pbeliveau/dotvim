call pathogen#helptags()
execute pathogen#infect()
set number
syntax on
filetype plugin indent on
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
:nnoremap <F5> :set hlsearch! hlsearch?<CR>
" color corporation
color wal
let mapleader=","

" ledger vimrc
let g:ledger_bin = 'ledger'
let g:ledger_fold_blanks = 0
let g:ledger_maxwidth = 80
let g:ledger_decimal_sep = '.'
let g:ledger_default_commodity = '$'
let g:ledger_align_at = 60
let g:ledger_fillstring = '    -'
let g:ledger_detailed_first = 1
let g:ledger_extra_options = '--pedantic --explicit --check-payees'
au FileType ledger inoremap <silent> <Tab> <C-r>=ledger#autocomplete_and_align()<CR>
au FileType ledger vnoremap <silent> <Tab> :LedgerAlign<CR>

" swap are a danger for security sensitive files
set noswapfile

" NERDtree
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" spellchecking
nmap <silent> <leader>s :set spell!<CR>

" Note:  map! for Insert and Command-line, i.e. imap & cmap
"
map! ;a à
map! ;z â
map! ;c ç
map! ;d è
map! ;f ê
map! ;e é
map! ;i î
map! ;o ô
imap ;q «  »<Esc>hi
map! ;u ù
map! ;j û
